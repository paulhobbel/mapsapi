import {Db, MongoClient} from 'mongodb';
import { Collection } from 'mongoose';
import { Config } from './config';

//TODO: Update this class to use definitions from .env
//TODO: Move all the database logic to this class instead of the keys and authentication classes
export default class MongoDB {
    private client: MongoClient | undefined;

    private COLLECTION_NAMES : string[] = ['keys',  'users'];
    private DEFAULT_COLLECTION_VALUES : (object|undefined)[] = [undefined, {"username":"admin", "password":"admin", "admin":true}]

    private static instance: MongoDB;

    private host: string;
    private port: number;

    static getInstance() : MongoDB {
        if(!MongoDB.instance) {
            MongoDB.instance = new MongoDB();
        }

        return MongoDB.instance;
    }

    private constructor() {
        this.host = Config.get<string>("MONGODB_HOST", "127.0.0.1");
        this.port = Config.get<number>("MONGODB_PORT", 27017);
    }

    private async connect() {
        try{
            if(!this.client) {
                console.info("Connecting to mongoDB");
                this.client = await MongoClient.connect(`mongodb://${this.host}:${this.port}`, {useUnifiedTopology : true});
                await this.checkAndFixDb();
            }
        } catch(error) {
            console.error(error);
            console.error("Fatal error ocurred with the database, exiting now");
            process.exit(-1);
        }
    }

    public async getDb() : Promise<Db> {
        if(this.client) {
            console.info("Getting the database");
            return this.client.db("mapsapi");
        } else {
            console.error("Not connected");
            await this.connect();
            return this.getDb();
        }
    }

    private async checkAndFixDb() {
        console.log("Checking the status of the database");
        let db : Db = await this.getDb();
        
        try{
            let collections : Collection[] = await db.listCollections().toArray();
            for(let i = 0; i < this.COLLECTION_NAMES.length; i++) {
                let contains : boolean = false;
                let collectionName : string = this.COLLECTION_NAMES[i];

                for(var j = 0 ; j < collections.length && !contains; j ++) {
                    contains = collections[j].name == collectionName;
                }

                if(!contains && db) {
                    console.log(`Creating a collection with the name ${collectionName.toString()}`)
                    db.createCollection(collectionName);
                    if(this.DEFAULT_COLLECTION_VALUES[i]) {
                        console.log(`Inserting ${this.DEFAULT_COLLECTION_VALUES[i]} into collection ${collectionName}`);
                        let collection : any = await db.collection(collectionName);
                        collection.insertOne(this.DEFAULT_COLLECTION_VALUES[i]);
                    }
                }
            }
        } catch(error) {
            console.log(error);
        }

        console.log("Database is valid");
    }
}