import Redis, { RedisClient, ClientOpts } from 'redis';

interface QuotaOptions extends ClientOpts {
  host: string;
  prefix?: string,
  limit: number;
};

export class Quota {

  private options: QuotaOptions;
  private redisClient: RedisClient;

  constructor(options: QuotaOptions) {
    this.redisClient = Redis.createClient(options);

    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    this.options = {
      prefix: 'quotas',
      ...options
    }
  }

  get limit() {
    return this.options.limit;
  }

  left(id: string): Promise<number> {
    return new Promise((resolve, reject) => {
      const key = [this.options.prefix, id].join(':');

      this.redisClient.get(key, (err, reply) => {
        if(err) return reject(err);

        if(!reply) {
          return resolve(this.options.limit);
        }

        const quota = Number(reply);
        return resolve(!isNaN(quota) && quota > 0 ? quota : 0);
      });
    });
  }

  set(id: string, amount:number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const key = [this.options.prefix, id].join(':');
      if(this.redisClient.set(key, amount.toString()))
        resolve(true);
    });
  }

  check(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const key = [this.options.prefix, id].join(':');

      this.redisClient.keys(key, async (err, reply) => {
        if(err) return reject(err);
          
        if(reply.length == 0) {
          await this.createKey(key);
        }

        this.redisClient.decr(key, (err, reply) => {
          if(err) return reject(err);

          return resolve(reply >= 0);
        })
      });
    });
  }

  private createKey(key: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.redisClient.set(key, this.options.limit.toString(), (err, reply) => {
        if(err) return reject(err);

        this.redisClient.expireat(key, Math.round(this.getTomorrowDate().getTime() / 1000), (err, reply) => {
          if(err) return reject(err);

          return resolve();
        });
      })
    });
  }

  private getTomorrowDate(): Date {
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    tomorrow.setSeconds(0);
    tomorrow.setMinutes(0);
    tomorrow.setHours(0);

    return tomorrow;
  }
}