import * as dotenv from 'dotenv';

dotenv.config();

export class Config {
  static get<T>(key: string, fallback?: T): T {
    const value = process.env[key] as any;

    if(value && !isNaN(Number(value))) {
      return Number(value) as any;
    }

    return value || fallback;
  }
}