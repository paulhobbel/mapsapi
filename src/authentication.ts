import { Request, ResponseToolkit } from "@hapi/hapi";
import MongoDB from './mongodb'
import { Db, ObjectId } from "mongodb";

type Credentials = {
    id: string,
    name: string
}

type Authorization = {
    isValid: boolean,
    credentials: Credentials | null
}

type User = {
    _id: string,
    username: string,
    password: string,
    isAdmin: boolean
}

export class Authentication {

    private mongoDb: MongoDB;
    private database : Db | undefined;

    constructor() {
        this.mongoDb = MongoDB.getInstance();
    }

    async authenticate(request : Request, username: string, password: string, h: ResponseToolkit) : Promise<Authorization> { 
        if(!this.database)
            this.database = await this.mongoDb.getDb();
        
        let users: User[] = await this.database.collection('users').find().toArray();
        for(let i = 0; i < users.length; i++) {
            if(users[i].username == username && users[i].password == password) {
                return {isValid : true, credentials: {id: users[i]._id, name: users[i].username}};
            }
        }

        return { credentials: null, isValid: false };
    }

}