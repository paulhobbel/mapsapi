import Boom from "@hapi/boom";
import axios from 'axios';
import qs from 'querystring';
import { Server, Request, ResponseToolkit } from '@hapi/hapi';

import { get } from 'config';
import { Config } from './config';
import { Quota } from './quota';
import { Keys } from './keys';
import { Authentication } from './authentication'

type DirectionsQuery = {
  key: string
};

type ConfigKey = {
  id?: string;
  name: string;
  key: string;
  quota?: number;
}

const secretKey = Config.get<string>('SECRET_KEY');
const baseUrl = 'https://maps.googleapis.com/maps/api/directions/json';
const quota = new Quota({
  host: Config.get('REDIS_HOST', '127.0.0.1'),
  port: Config.get('REDIS_PORT', 6379),
  limit: Config.get('QUOTA_LIMIT', 400)
})

const keys = new Keys();
const authentication = new Authentication();


if(!secretKey) {
  throw new Error('SECRET_KEY has not been set');
}

async function isValidKey(key: string) {
  console.log("isvalidkey");
  return key && await keys.checkValidKey(key);
}

async function main() {

  const server = new Server({
    port: Config.get('SERVER_PORT', 3000)
  });

  await server.register(require('@hapi/basic'));

  server.auth.strategy('simple', 'basic', {validate: async(request : Request, username: string, password: string, h: ResponseToolkit) => authentication.authenticate(request, username, password, h)});

  server.route({
    method: 'GET',
    path: '/directions',
    handler: async (req: Request, h: ResponseToolkit) => {
      console.log("GET /directions");
      try {
        const query = { ...req.query as DirectionsQuery };

        if(!await isValidKey(query.key)) {
          return Boom.unauthorized("Invalid api key");
        }

        if(!await quota.check(query.key)) {
          return Boom.tooManyRequests(`Quota limit of ${quota.limit} reached`);
        }

        query.key = secretKey;

        try {
          const response = await axios.get(`${baseUrl}?${qs.stringify(query)}`);

          return response.data;
        } catch(error) {
          return error.response.data;
        }

      } catch (error) {
        console.error(error);

        return Boom.internal(error);
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/quota',
    handler: async (req: Request) => {
      console.log("GET /quota");
      try {
        const { key } = req.query as DirectionsQuery;

        if(!isValidKey(key)) {
          return Boom.unauthorized("Invalid api key");
        }

        const left = await quota.left(key);

        return {
          quota: left
        }
      } catch(error) {
        console.error(error);

        return Boom.internal(error);
      }
    }
  })

  server.route({
    method: 'GET',
    path: '/keys/{key*}',
    options: {
      cors: true,
      auth: 'simple'
    },
    handler: async (req: Request) => {
      console.log(`GET /keys/${req.params.key ? req.params.key : ''}`);
      let k = await keys.getallKeys();

      if(req.params.key) {
        for(let i = 0; i < k.length; i++) {
          if(k[i]._id == req.params.key) {
            let _quota = await quota.left(k[i].key); 
            return {"data": {id: k[i]._id, name: k[i].name, key: k[i].key, quota: _quota}};
          }
        }
      } else {
        let _keys : ConfigKey[] = [];
        
        for(let i = 0; i < k.length; i++) {
          let _quota = await quota.left(k[i].key);
          _keys[i] = {id: k[i]._id, name: k[i].name, key: k[i].key, quota: _quota};
        }

        return {
          "data": _keys
        }
      }
    }
  });

  server.route({
    method: 'POST',
    path: '/keys',
    options: {
      cors: true,
      auth: 'simple'
    },
    handler: async (req: Request, h: ResponseToolkit) => {
      console.log("POST /directions");
      if(!(req.payload as object))
        return Boom.badData("Payload has to be a json object");
      
      let key : any = req.payload;
      await keys.addKey(key);
      let _quota = await quota.left(key.key); 
      return {"data": {id: key._id, name: key.name, key: key.key, quota: _quota}};
    }
  });

  server.route({
    method: 'PUT',
    path: '/keys/{key*}',
    options: {
      cors: true,
      auth: 'simple'
    },
    handler: async (req: Request, h: ResponseToolkit) => {
      console.log(`PUT /keys/${req.params.key ? req.params.key : ''}`);
      if(!(req.payload as object))
        return Boom.badData("Payload has to be a json object");
      
      let payload : any = req.payload;

      await quota.set(payload.key, payload.quota);

      let _quota = await quota.left(payload.key); 
      return {"data": {id: payload.id, name: payload.name, key: payload.key, quota: _quota}};
    }
  });

  server.route({
    method: 'DELETE',
    path: '/keys/{key*}',
    options: {
      cors: true,
      auth: 'simple'
    },
    handler: async (req: Request, h: ResponseToolkit) => {
      console.log(`DELETE /keys/${req.params.key ? req.params.key : ''}`);
      keys.deleteKey(req.params.key);

      return h.response().code(200);
    }
  });

  server.start().then(async () => {
    console.log(`Server is listening at ${server.info.uri}`);  
  }).catch(console.error);
}

main();