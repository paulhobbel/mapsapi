import MongoDB from './mongodb'
import { Db, ObjectId } from 'mongodb';

type Key = {
    _id?: string,
    name : string,
    key : string
};

export class Keys {

    private mongoDb : MongoDB;
    private database : Db | undefined;

    constructor() {
        this.mongoDb = MongoDB.getInstance();
    }

    async checkValidKey(key : string) : Promise<boolean> {
        if(!this.database)
            this.database = await this.mongoDb.getDb();

        let keys : Key[] = await this.database.collection('keys').find().toArray();
        for(let i = 0; i < keys.length; i ++) {
            if(keys[i].key == key)
                return true;
        }
        return false;
    }

    async getallKeys() : Promise<Key[]> {
        if(!this.database)
            this.database = await this.mongoDb.getDb();
        return await this.database.collection('keys').find().toArray();
    }

    async addKey(key : Key) {
        if(!this.database)
            this.database = await this.mongoDb.getDb();
        await this.database.collection('keys').insertOne(key);
    }

    async deleteKey(id: string) {
        if(!this.database)
            this.database = await this.mongoDb.getDb();
        
        await this.database.collection('keys').deleteOne({"_id": new ObjectId(id)});
    }

}