FROM node:11.3-alpine

LABEL maintainer="Paul Hobbel <hello@paulhobbel.me>"

WORKDIR /app
ADD . .

RUN npm install --production

CMD ["npm", "start"]